#!/usr/bin/python3

import argparse
import itertools
import re
import os
import subprocess
import time
from threading import Lock, Thread, Timer
from typing import Optional, List, Tuple
import requests
import json
from flask import Flask


class Watchdog:
    def __init__(self, timeout: float, callback, *args) -> None:
        self.__timeout = timeout
        self.__callback = callback
        self.__args = args
        self.__timer = None

    def start(self):
        self.__timer = Timer(self.__timeout, self.__fireRepeat)
        self.__timer.start()

    def reset(self):
        if self.__timer:
            self.__timer.cancel()
            self.start()

    def fireNow(self):
        if self.__timer:
            self.__timer.cancel()
        self.__fire()
        if self.__timer:
            self.start()

    def stop(self):
        if self.__timer:
            self.__timer.cancel()
            self.__timer = None

    def __fireRepeat(self):
        self.__fire()
        self.start()

    def __fire(self):
        self.__callback(*self.__args)


def validateSSHHost(val):
    reg = re.compile('[a-zA-Z0-9\\-_]+@[a-zA-Z0-9\\-_.]+')
    if not reg.match(val):
        raise ValueError("SSH host not valid")
    return val


def validateServicePort(val):
    reg = re.compile('[0-9]+:[0-9]+')
    if not reg.match(val):
        raise ValueError("SSH host not valid")
    [local, remote] = val.split(':')
    return int(local), int(remote)


parser = argparse.ArgumentParser()
parser.add_argument('--http-server-path',
                    help='The HTTP path where the local HTTP heartbeat server will be started',
                    type=str, required=True)
parser.add_argument('--http-server-port',
                    help='The port where to start the local HTTP heartbeat server',
                    type=str, required=True)
parser.add_argument('--http-remote-port',
                    help='The port where to tunnel the HTTP heartbeat server',
                    type=str, required=True)
parser.add_argument('--http-check-url',
                    help='The URL where to check existence of heartbeat',
                    type=str, required=True)
parser.add_argument('--remote-ssh-host',
                    help='The remote username@host used for SSH tunneling',
                    type=validateSSHHost, required=True)
parser.add_argument('--remote-ssh-key',
                    help='The SSH key used for tunneling',
                    type=str, required=True)
parser.add_argument('--service', '-S',
                    help='The local to remote port to tunnel, in local:remote format',
                    type=validateServicePort, required=True, action='append')


sshproc: Optional[subprocess.Popen] = None
tunnelRestarter: Watchdog
tunnelChecker: Watchdog
operationLock: Lock = Lock()
flaskApp = Flask(__name__)


def refreshSSHTunnel(ports: List[Tuple[int, int]], host, key):
    global sshproc
    operationLock.acquire()
    if sshproc:
        try:
            os.kill(sshproc.pid, 0)
        except OSError:
            sshproc = None
    if sshproc:
        print('Killing tunnel with pid {}'.format(sshproc.pid))
        os.kill(sshproc.pid, 2)
        try:
            sshproc.communicate(timeout=5)
        except subprocess.TimeoutExpired:
            os.kill(sshproc.pid, 9)
            sshproc.communicate()
        sshproc = None
    cmdline = ['ssh'] \
              + list(itertools.chain.from_iterable([['-R', '{}:localhost:{}'.format(remote, local)] for (local, remote) in ports])) \
              + [host] + ['-i', key] \
              + ['-o', 'ServerAliveInterval 30'] \
              + ['-o', 'ServerAliveCountMax 3'] \
              + ['-N']
    sshproc = subprocess.Popen(cmdline)
    print('Started tunnel with pid {}'.format(sshproc.pid))
    operationLock.release()


def checkTunnel(location: str):
    operationLock.acquire()
    try:
        req = requests.get(location, timeout=5)
        if req.status_code != 200:
            raise Exception
        jsonData = req.json()
        time = jsonData['time']
        tunnelRestarter.reset()
        print("OK")
    except:
        print("Failed to get heartbeat")
    operationLock.release()


def startFlaskApp(route, port):
    @flaskApp.route(route)
    def getHeartbeat():
        return json.dumps({'time': time.time()})
    flaskApp.run(port=port)


if __name__ == '__main__':
    args = parser.parse_args()

    Thread(target=lambda: startFlaskApp(args.http_server_path, args.http_server_port)).start()

    tunnelPorts = args.service + [(args.http_server_port, args.http_remote_port)]
    tunnelRestarter = Watchdog(30, refreshSSHTunnel, tunnelPorts, args.remote_ssh_host, args.remote_ssh_key)
    tunnelChecker = Watchdog(3, checkTunnel, args.http_check_url)
    tunnelRestarter.fireNow()
    tunnelRestarter.start()
    tunnelChecker.start()

