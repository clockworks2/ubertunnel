#!/bin/bash

EXECT='/usr/bin/ubertunnel.py'
SYSTEM='/etc/systemd/system/ubertunnel.service'
ARGS='/etc/ubertunnel.args'

[ -f $EXECT ] && rm -f $EXECT
[ -f $SYSTEM ] && rm -f $SYSTEM
[ -f $ARGS ] && rm -f $ARGS

cp ubertunnel.py $EXECT
cp ubertunnel.service $SYSTEM
cp ubertunnel.args $ARGS

chmod a+x $EXECT
chmod a+x $SYSTEM

